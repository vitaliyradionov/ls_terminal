
--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = off;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET escape_string_warning = off;

SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: bet; Type: TABLE; Schema: public; Owner: -; Tablespace:
--

CREATE TABLE bet (
    tag character varying(20) NOT NULL,
    name character varying(80),
    bettype character varying(16),
    scoretype character varying(4),
    timetype character varying(5),
    description character varying(256),
    mapping_code integer,
    xml xml,
    enabled boolean DEFAULT false
);


--
-- Name: bet_type; Type: TABLE; Schema: public; Owner: -; Tablespace:
--

CREATE TABLE bet_type (
    shortname character(16) NOT NULL,
    name character varying(128),
    description character varying(256)
);


--
-- Name: score_type; Type: TABLE; Schema: public; Owner: -; Tablespace:
--

CREATE TABLE score_type (
    shortname character varying(4) NOT NULL,
    name character varying(128),
    description character varying(256),
    split character(1)
);


--
-- Name: time_type; Type: TABLE; Schema: public; Owner: -; Tablespace:
--

CREATE TABLE time_type (
    tag character varying(5) NOT NULL,
    name character varying(128),
    description character varying(256),
    enable boolean,
    xml xml
);


delete from bet;
delete from bet_type;
delete from score_type;
delete from time_type;


--
-- Data for Name: bet; Type: TABLE DATA; Schema: public; Owner: -
--


INSERT INTO bet_type (shortname, name, description) VALUES ('TTL', 'Тотал', 'Ставка на сумму счёта (тотал)');
INSERT INTO bet_type (shortname, name, description) VALUES ('WIN', 'Победитель', 'Ставка на победителя');
INSERT INTO bet_type (shortname, name, description) VALUES ('OTH', 'Другое', 'Другое');
INSERT INTO bet_type (shortname, name, description) VALUES ('ERR', 'Другое', 'Отмечает ошибку при обработке пари');
INSERT INTO bet_type (shortname, name, description) VALUES ('EXA', 'Точное значение', 'Точное значение счета');
INSERT INTO bet_type (shortname, name, description) VALUES ('ODD', 'Чет/нечет', 'Чет/нечет');
INSERT INTO bet_type (shortname, name, description) VALUES ('DBL', 'Двойной шанс', 'Ставка на исходы 1X 12 2X');
INSERT INTO bet_type (shortname, name, description) VALUES ('HAN', 'Фора', 'Фора');
INSERT INTO bet_type (shortname, name, description) VALUES ('NXG', 'Кто забьет следующий гол', 'Кто забьет следующий гол');
INSERT INTO bet_type (shortname, name, description) VALUES ('WTL', 'Победитель + Тотал', 'Ставка на победителя и тотал');

--
-- TOC entry 2089 (class 0 OID 304399)
-- Dependencies: 203
-- Data for Name: score_type; Type: TABLE DATA; Schema: static; Owner: black
--

INSERT INTO score_type (shortname, name, description, split) VALUES ('CON', 'Угловые', 'Угловые', 'y');
INSERT INTO score_type (shortname, name, description, split) VALUES ('PNL', 'Пенальти', 'Пенальти', 'y');
INSERT INTO score_type (shortname, name, description, split) VALUES ('SCR', 'Голы', 'Голы', 'y');
INSERT INTO score_type (shortname, name, description, split) VALUES ('SET', 'Число сетов', 'Число сетов', 'n');
INSERT INTO score_type (shortname, name, description, split) VALUES ('YCR', 'Желтые карточки', 'Желтые карточки', 'y');
INSERT INTO score_type (shortname, name, description, split) VALUES ('RCR', 'Красные карточки', 'Красные карточки', 'y');


--
-- TOC entry 2088 (class 0 OID 304391)
-- Dependencies: 202
-- Data for Name: time_type; Type: TABLE DATA; Schema: static; Owner: black
--

INSERT INTO time_type (tag, name, description, enable, xml) VALUES ('GM', 'Гейм', 'Гейм', false, NULL);
INSERT INTO time_type (tag, name, description, enable, xml) VALUES ('TIM', 'Тайм', 'Тайм', NULL, NULL);
INSERT INTO time_type (tag, name, description, enable, xml) VALUES ('GAM', 'Весь матч', 'Весь матч', NULL, NULL);
INSERT INTO time_type (tag, name, description, enable, xml) VALUES ('WO', 'Досрочная победа', 'Один из игроков не может закончить игру', false, NULL);
INSERT INTO time_type (tag, name, description, enable, xml) VALUES ('P1', 'Первый период', 'Первый период', false, NULL);
INSERT INTO time_type (tag, name, description, enable, xml) VALUES ('P2', 'Второй период', 'Второй период', false, NULL);
INSERT INTO time_type (tag, name, description, enable, xml) VALUES ('P3', 'Третий период', 'Третий период', false, NULL);
INSERT INTO time_type (tag, name, description, enable, xml) VALUES ('Q1', 'Первая четверть', 'Первая четверть', false, NULL);
INSERT INTO time_type (tag, name, description, enable, xml) VALUES ('Q2', 'Вторая четверть', 'Вторая четверть', false, NULL);
INSERT INTO time_type (tag, name, description, enable, xml) VALUES ('Q3', 'Третья четверть', 'Третья четверть', false, NULL);
INSERT INTO time_type (tag, name, description, enable, xml) VALUES ('Q4', 'Четвертая четверть', 'Четвертая четверть', false, NULL);
INSERT INTO time_type (tag, name, description, enable, xml) VALUES ('FT', 'Основное время', 'Основное время', false, NULL);
INSERT INTO time_type (tag, name, description, enable, xml) VALUES ('EXT', 'Дополнительное время', 'Дополнительное время', false, NULL);
INSERT INTO time_type (tag, name, description, enable, xml) VALUES ('OT', 'Овертайм', 'Игра в дополнительное время', false, NULL);
INSERT INTO time_type (tag, name, description, enable, xml) VALUES ('F10', '10 фрейм', '10 фрейм', NULL, NULL);
INSERT INTO time_type (tag, name, description, enable, xml) VALUES ('F11', '11 фрейм', '11 фрейм', NULL, NULL);
INSERT INTO time_type (tag, name, description, enable, xml) VALUES ('F12', '12 фрейм', '12 фрейм', NULL, NULL);
INSERT INTO time_type (tag, name, description, enable, xml) VALUES ('F13', '13 фрейм', '13 фрейм', NULL, NULL);
INSERT INTO time_type (tag, name, description, enable, xml) VALUES ('F14', '14 фрейм', '14 фрейм', NULL, NULL);
INSERT INTO time_type (tag, name, description, enable, xml) VALUES ('F15', '15 фрейм', '15 фрейм', NULL, NULL);
INSERT INTO time_type (tag, name, description, enable, xml) VALUES ('F16', '16 фрейм', '16 фрейм', NULL, NULL);
INSERT INTO time_type (tag, name, description, enable, xml) VALUES ('F17', '17 фрейм', '17 фрейм', NULL, NULL);
INSERT INTO time_type (tag, name, description, enable, xml) VALUES ('F18', '18 фрейм', '18 фрейм', NULL, NULL);
INSERT INTO time_type (tag, name, description, enable, xml) VALUES ('F19', '19 фрейм', '19 фрейм', NULL, NULL);
INSERT INTO time_type (tag, name, description, enable, xml) VALUES ('F20', '20 фрейм', '20 фрейм', NULL, NULL);
INSERT INTO time_type (tag, name, description, enable, xml) VALUES ('F21', '21 фрейм', '21 фрейм', NULL, NULL);
INSERT INTO time_type (tag, name, description, enable, xml) VALUES ('F22', '22 фрейм', '22 фрейм', NULL, NULL);
INSERT INTO time_type (tag, name, description, enable, xml) VALUES ('F23', '23 фрейм', '23 фрейм', NULL, NULL);
INSERT INTO time_type (tag, name, description, enable, xml) VALUES ('F24', '24 фрейм', '24 фрейм', NULL, NULL);
INSERT INTO time_type (tag, name, description, enable, xml) VALUES ('F25', '25 фрейм', '25 фрейм', NULL, NULL);
INSERT INTO time_type (tag, name, description, enable, xml) VALUES ('F02', '2 фрейм', '2 фрейм', NULL, NULL);
INSERT INTO time_type (tag, name, description, enable, xml) VALUES ('F03', '3 фрейм', '3 фрейм', NULL, NULL);
INSERT INTO time_type (tag, name, description, enable, xml) VALUES ('F04', '4 фрейм', '4 фрейм', NULL, NULL);
INSERT INTO time_type (tag, name, description, enable, xml) VALUES ('F05', '5 фрейм', '5 фрейм', NULL, NULL);
INSERT INTO time_type (tag, name, description, enable, xml) VALUES ('F06', '6 фрейм', '6 фрейм', NULL, NULL);
INSERT INTO time_type (tag, name, description, enable, xml) VALUES ('F07', '7 фрейм', '7 фрейм', NULL, NULL);
INSERT INTO time_type (tag, name, description, enable, xml) VALUES ('F08', '8 фрейм', '8 фрейм', NULL, NULL);
INSERT INTO time_type (tag, name, description, enable, xml) VALUES ('F09', '9 фрейм', '9 фрейм', NULL, NULL);
INSERT INTO time_type (tag, name, description, enable, xml) VALUES ('S1', 'Первый сет', 'Первый сет', false, NULL);
INSERT INTO time_type (tag, name, description, enable, xml) VALUES ('S2', 'Второй сет', 'Второй сет', false, NULL);
INSERT INTO time_type (tag, name, description, enable, xml) VALUES ('S3', 'Третий сет', 'Третий сет', false, NULL);
INSERT INTO time_type (tag, name, description, enable, xml) VALUES ('S4', 'Четвертый сет', 'Четвертый сет', false, NULL);
INSERT INTO time_type (tag, name, description, enable, xml) VALUES ('S5', 'Пятый сет', 'Пятый сет', false, NULL);
INSERT INTO time_type (tag, name, description, enable, xml) VALUES ('H1', 'Первая половина', 'Первая половина матча', false, NULL);
INSERT INTO time_type (tag, name, description, enable, xml) VALUES ('H2', 'Вторая половина', 'Вторая половина', false, NULL);
INSERT INTO time_type (tag, name, description, enable, xml) VALUES ('F01', '1 фрейм', '1 фрейм', true, NULL);
INSERT INTO time_type (tag, name, description, enable, xml) VALUES ('AP', 'Пенальти', 'Пенальти и буллиты', false, NULL);
INSERT INTO time_type (tag, name, description, enable, xml) VALUES ('S1G01', '1-й гейм 1-го сета', '1-й гейм 1-го сета', NULL, NULL);
INSERT INTO time_type (tag, name, description, enable, xml) VALUES ('S1G02', '2-й гейм 1-го сета', '2-й гейм 1-го сета', NULL, NULL);
INSERT INTO time_type (tag, name, description, enable, xml) VALUES ('S1G03', '3-й гейм 1-го сета', '3-й гейм 1-го сета', NULL, NULL);
INSERT INTO time_type (tag, name, description, enable, xml) VALUES ('S1G04', '4-й гейм 1-го сета', '4-й гейм 1-го сета', NULL, NULL);
INSERT INTO time_type (tag, name, description, enable, xml) VALUES ('S1G05', '5-й гейм 1-го сета', '5-й гейм 1-го сета', NULL, NULL);
INSERT INTO time_type (tag, name, description, enable, xml) VALUES ('S1G06', '6-й гейм 1-го сета', '6-й гейм 1-го сета', NULL, NULL);
INSERT INTO time_type (tag, name, description, enable, xml) VALUES ('S1G07', '7-й гейм 1-го сета', '7-й гейм 1-го сета', NULL, NULL);
INSERT INTO time_type (tag, name, description, enable, xml) VALUES ('S1G08', '8-й гейм 1-го сета', '8-й гейм 1-го сета', NULL, NULL);
INSERT INTO time_type (tag, name, description, enable, xml) VALUES ('S1G09', '9-й гейм 1-го сета', '9-й гейм 1-го сета', NULL, NULL);
INSERT INTO time_type (tag, name, description, enable, xml) VALUES ('S1G10', '10-й гейм 1-го сета', '10-й гейм 1-го сета', NULL, NULL);
INSERT INTO time_type (tag, name, description, enable, xml) VALUES ('S1G11', '11-й гейм 1-го сета', '11-й гейм 1-го сета', NULL, NULL);
INSERT INTO time_type (tag, name, description, enable, xml) VALUES ('S1G12', '12-й гейм 1-го сета', '12-й гейм 1-го сета', NULL, NULL);
INSERT INTO time_type (tag, name, description, enable, xml) VALUES ('S2G01', '1-й гейм 2-го сета', '1-й гейм 2-го сета', NULL, NULL);
INSERT INTO time_type (tag, name, description, enable, xml) VALUES ('S2G02', '2-й гейм 2-го сета', '2-й гейм 2-го сета', NULL, NULL);
INSERT INTO time_type (tag, name, description, enable, xml) VALUES ('S2G03', '3-й гейм 2-го сета', '3-й гейм 2-го сета', NULL, NULL);
INSERT INTO time_type (tag, name, description, enable, xml) VALUES ('S2G04', '4-й гейм 2-го сета', '4-й гейм 2-го сета', NULL, NULL);
INSERT INTO time_type (tag, name, description, enable, xml) VALUES ('S2G05', '5-й гейм 2-го сета', '5-й гейм 2-го сета', NULL, NULL);
INSERT INTO time_type (tag, name, description, enable, xml) VALUES ('S2G06', '6-й гейм 2-го сета', '6-й гейм 2-го сета', NULL, NULL);
INSERT INTO time_type (tag, name, description, enable, xml) VALUES ('S2G07', '7-й гейм 2-го сета', '7-й гейм 2-го сета', NULL, NULL);
INSERT INTO time_type (tag, name, description, enable, xml) VALUES ('S2G08', '8-й гейм 2-го сета', '8-й гейм 2-го сета', NULL, NULL);
INSERT INTO time_type (tag, name, description, enable, xml) VALUES ('S2G09', '9-й гейм 2-го сета', '9-й гейм 2-го сета', NULL, NULL);
INSERT INTO time_type (tag, name, description, enable, xml) VALUES ('S2G10', '10-й гейм 2-го сета', '10-й гейм 2-го сета', NULL, NULL);
INSERT INTO time_type (tag, name, description, enable, xml) VALUES ('S2G11', '11-й гейм 2-го сета', '11-й гейм 2-го сета', NULL, NULL);
INSERT INTO time_type (tag, name, description, enable, xml) VALUES ('S2G12', '12-й гейм 2-го сета', '12-й гейм 2-го сета', NULL, NULL);
INSERT INTO time_type (tag, name, description, enable, xml) VALUES ('S3G01', '1-й гейм 3-го сета', '1-й гейм 3-го сета', NULL, NULL);
INSERT INTO time_type (tag, name, description, enable, xml) VALUES ('S3G02', '2-й гейм 3-го сета', '2-й гейм 3-го сета', NULL, NULL);
INSERT INTO time_type (tag, name, description, enable, xml) VALUES ('S3G03', '3-й гейм 3-го сета', '3-й гейм 3-го сета', NULL, NULL);
INSERT INTO time_type (tag, name, description, enable, xml) VALUES ('S3G04', '4-й гейм 3-го сета', '4-й гейм 3-го сета', NULL, NULL);
INSERT INTO time_type (tag, name, description, enable, xml) VALUES ('S3G05', '5-й гейм 3-го сета', '5-й гейм 3-го сета', NULL, NULL);
INSERT INTO time_type (tag, name, description, enable, xml) VALUES ('S3G06', '6-й гейм 3-го сета', '6-й гейм 3-го сета', NULL, NULL);
INSERT INTO time_type (tag, name, description, enable, xml) VALUES ('S3G07', '7-й гейм 3-го сета', '7-й гейм 3-го сета', NULL, NULL);
INSERT INTO time_type (tag, name, description, enable, xml) VALUES ('S3G08', '8-й гейм 3-го сета', '8-й гейм 3-го сета', NULL, NULL);
INSERT INTO time_type (tag, name, description, enable, xml) VALUES ('S3G09', '9-й гейм 3-го сета', '9-й гейм 3-го сета', NULL, NULL);
INSERT INTO time_type (tag, name, description, enable, xml) VALUES ('S3G10', '10-й гейм 3-го сета', '10-й гейм 3-го сета', NULL, NULL);
INSERT INTO time_type (tag, name, description, enable, xml) VALUES ('S3G11', '11-й гейм 3-го сета', '11-й гейм 3-го сета', NULL, NULL);
INSERT INTO time_type (tag, name, description, enable, xml) VALUES ('S3G12', '12-й гейм 3-го сета', '12-й гейм 3-го сета', NULL, NULL);
INSERT INTO time_type (tag, name, description, enable, xml) VALUES ('S4G01', '1-й гейм 4-го сета', '1-й гейм 4-го сета', NULL, NULL);
INSERT INTO time_type (tag, name, description, enable, xml) VALUES ('S4G02', '2-й гейм 4-го сета', '2-й гейм 4-го сета', NULL, NULL);
INSERT INTO time_type (tag, name, description, enable, xml) VALUES ('S4G03', '3-й гейм 4-го сета', '3-й гейм 4-го сета', NULL, NULL);
INSERT INTO time_type (tag, name, description, enable, xml) VALUES ('S4G04', '4-й гейм 4-го сета', '4-й гейм 4-го сета', NULL, NULL);
INSERT INTO time_type (tag, name, description, enable, xml) VALUES ('S4G05', '5-й гейм 4-го сета', '5-й гейм 4-го сета', NULL, NULL);
INSERT INTO time_type (tag, name, description, enable, xml) VALUES ('S4G06', '6-й гейм 4-го сета', '6-й гейм 4-го сета', NULL, NULL);
INSERT INTO time_type (tag, name, description, enable, xml) VALUES ('S4G07', '7-й гейм 4-го сета', '7-й гейм 4-го сета', NULL, NULL);
INSERT INTO time_type (tag, name, description, enable, xml) VALUES ('S4G08', '8-й гейм 4-го сета', '8-й гейм 4-го сета', NULL, NULL);
INSERT INTO time_type (tag, name, description, enable, xml) VALUES ('S4G09', '9-й гейм 4-го сета', '9-й гейм 4-го сета', NULL, NULL);
INSERT INTO time_type (tag, name, description, enable, xml) VALUES ('S4G10', '10-й гейм 4-го сета', '10-й гейм 4-го сета', NULL, NULL);
INSERT INTO time_type (tag, name, description, enable, xml) VALUES ('S4G11', '11-й гейм 4-го сета', '11-й гейм 4-го сета', NULL, NULL);
INSERT INTO time_type (tag, name, description, enable, xml) VALUES ('S4G12', '12-й гейм 4-го сета', '12-й гейм 4-го сета', NULL, NULL);
INSERT INTO time_type (tag, name, description, enable, xml) VALUES ('S5G01', '1-й гейм 5-го сета', '1-й гейм 5-го сета', NULL, NULL);
INSERT INTO time_type (tag, name, description, enable, xml) VALUES ('S5G02', '2-й гейм 5-го сета', '2-й гейм 5-го сета', NULL, NULL);
INSERT INTO time_type (tag, name, description, enable, xml) VALUES ('S5G03', '3-й гейм 5-го сета', '3-й гейм 5-го сета', NULL, NULL);
INSERT INTO time_type (tag, name, description, enable, xml) VALUES ('S5G04', '4-й гейм 5-го сета', '4-й гейм 5-го сета', NULL, NULL);
INSERT INTO time_type (tag, name, description, enable, xml) VALUES ('S5G05', '5-й гейм 5-го сета', '5-й гейм 5-го сета', NULL, NULL);
INSERT INTO time_type (tag, name, description, enable, xml) VALUES ('S5G06', '6-й гейм 5-го сета', '6-й гейм 5-го сета', NULL, NULL);
INSERT INTO time_type (tag, name, description, enable, xml) VALUES ('S5G07', '7-й гейм 5-го сета', '7-й гейм 5-го сета', NULL, NULL);
INSERT INTO time_type (tag, name, description, enable, xml) VALUES ('S5G08', '8-й гейм 5-го сета', '8-й гейм 5-го сета', NULL, NULL);
INSERT INTO time_type (tag, name, description, enable, xml) VALUES ('S5G09', '9-й гейм 5-го сета', '9-й гейм 5-го сета', NULL, NULL);
INSERT INTO time_type (tag, name, description, enable, xml) VALUES ('S5G10', '10-й гейм 5-го сета', '10-й гейм 5-го сета', NULL, NULL);
INSERT INTO time_type (tag, name, description, enable, xml) VALUES ('S5G11', '11-й гейм 5-го сета', '11-й гейм 5-го сета', NULL, NULL);
INSERT INTO time_type (tag, name, description, enable, xml) VALUES ('S5G12', '12-й гейм 5-го сета', '12-й гейм 5-го сета', NULL, NULL);
INSERT INTO time_type (tag, name, description, enable, xml) VALUES ('F26', '26 фрейм', '26 фрейм', NULL, NULL);
INSERT INTO time_type (tag, name, description, enable, xml) VALUES ('F27', '27 фрейм', '27 фрейм', NULL, NULL);
INSERT INTO time_type (tag, name, description, enable, xml) VALUES ('F28', '28 фрейм', '28 фрейм', NULL, NULL);
INSERT INTO time_type (tag, name, description, enable, xml) VALUES ('F29', '29 фрейм', '29 фрейм', NULL, NULL);
INSERT INTO time_type (tag, name, description, enable, xml) VALUES ('F30', '30 фрейм', '30 фрейм', NULL, NULL);
INSERT INTO time_type (tag, name, description, enable, xml) VALUES ('F31', '31 фрейм', '31 фрейм', NULL, NULL);
INSERT INTO time_type (tag, name, description, enable, xml) VALUES ('F32', '32 фрейм', '32 фрейм', NULL, NULL);
INSERT INTO time_type (tag, name, description, enable, xml) VALUES ('F33', '33 фрейм', '33 фрейм', NULL, NULL);
INSERT INTO time_type (tag, name, description, enable, xml) VALUES ('F34', '34 фрейм', '34 фрейм', NULL, NULL);
INSERT INTO time_type (tag, name, description, enable, xml) VALUES ('F35', '35 фрейм', '35 фрейм', NULL, NULL);
INSERT INTO time_type (tag, name, description, enable, xml) VALUES ('S6', 'Шестой сет', 'Шестой сет', false, NULL);
INSERT INTO time_type (tag, name, description, enable, xml) VALUES ('S7', 'Седьмой сет', 'Седьмой сет', false, NULL);
INSERT INTO time_type (tag, name, description, enable, xml) VALUES ('ERR', 'Неизвестно', 'Неизвестная часть матча', false, NULL);
INSERT INTO time_type (tag, name, description, enable, xml) VALUES ('I01', '1 иннинг', '1 иннинг', NULL, NULL);
INSERT INTO time_type (tag, name, description, enable, xml) VALUES ('I02', '2 иннинг', '2 иннинг', NULL, NULL);
INSERT INTO time_type (tag, name, description, enable, xml) VALUES ('I03', '3 иннинг', '3 иннинг', NULL, NULL);
INSERT INTO time_type (tag, name, description, enable, xml) VALUES ('I04', '4 иннинг', '4 иннинг', NULL, NULL);
INSERT INTO time_type (tag, name, description, enable, xml) VALUES ('I05', '5 иннинг', '5 иннинг', NULL, NULL);
INSERT INTO time_type (tag, name, description, enable, xml) VALUES ('I06', '6 иннинг', '6 иннинг', NULL, NULL);
INSERT INTO time_type (tag, name, description, enable, xml) VALUES ('I07', '7 иннинг', '7 иннинг', NULL, NULL);
INSERT INTO time_type (tag, name, description, enable, xml) VALUES ('I08', '8 иннинг', '8 иннинг', NULL, NULL);
INSERT INTO time_type (tag, name, description, enable, xml) VALUES ('I09', '9 иннинг', '9 иннинг', NULL, NULL);


--
-- TOC entry 2091 (class 0 OID 304410)
-- Dependencies: 205 2088 2090 2089
-- Data for Name: bet; Type: TABLE DATA; Schema: static; Owner: black
--
INSERT INTO bet (tag, name, bettype, scoretype, timetype, description, mapping_code, xml, enabled) VALUES ('WINFT', 'Победитель', 'WIN', 'SCR', 'FT', 'Ставка на победителя или ничью', 10, NULL, true);
--INSERT INTO bet (tag, name, bettype, scoretype, timetype, description, mapping_code, xml, enabled) VALUES ('WIN2', 'Победитель (2 исхода)', 'WIN', 'SCR', 'FT', 'Победитель 2 исхода', 20, NULL, true);
INSERT INTO bet (tag, name, bettype, scoretype, timetype, description, mapping_code, xml, enabled) VALUES ('ODDFT', 'Чет/нечет (основное время)', 'ODD', 'SCR', 'FT', 'Чет/нечет голов в основное время', -1, NULL, true);
INSERT INTO bet (tag, name, bettype, scoretype, timetype, description, mapping_code, xml, enabled) VALUES ('EXAFT', 'Точный счет (основное время)', 'EXA', 'SCR', 'FT', 'Точный счет в основное время', -1, NULL, true);
INSERT INTO bet (tag, name, bettype, scoretype, timetype, description, mapping_code, xml, enabled) VALUES ('ERRFT', 'Пари в основное время', 'ERR', 'SCR', 'FT', 'Пари в основное время', -1, NULL, true);
INSERT INTO bet (tag, name, bettype, scoretype, timetype, description, mapping_code, xml, enabled) VALUES ('OTHFT', 'Пари в основное время', 'OTH', 'SCR', 'FT', 'Пари в основное время', -1, NULL, true);
INSERT INTO bet (tag, name, bettype, scoretype, timetype, description, mapping_code, xml, enabled) VALUES ('OTHGAM', 'Пари матча', 'OTH', 'SCR', 'GAM', 'Пари матча', -1, NULL, true);
INSERT INTO bet (tag, name, bettype, scoretype, timetype, description, mapping_code, xml, enabled) VALUES ('ERRGAM', 'Пари матча', 'ERR', 'SCR', 'GAM', 'Пари матча', -1, NULL, true);
INSERT INTO bet (tag, name, bettype, scoretype, timetype, description, mapping_code, xml, enabled) VALUES ('HANGAM', 'Фора на весь матч', 'HAN', 'SCR', 'GAM', 'Фора на весь матч', -1, NULL, true);
INSERT INTO bet (tag, name, bettype, scoretype, timetype, description, mapping_code, xml, enabled) VALUES ('ERRERR', 'Пари', 'ERR', 'SCR', 'ERR', 'Пари', -1, NULL, false);
INSERT INTO bet (tag, name, bettype, scoretype, timetype, description, mapping_code, xml, enabled) VALUES ('OTHERR', 'Пари', 'OTH', 'SCR', 'ERR', 'Пари', -1, NULL, false);
INSERT INTO bet (tag, name, bettype, scoretype, timetype, description, mapping_code, xml, enabled) VALUES ('DBLERR', 'Двойной шанс', 'DBL', 'SCR', 'ERR', 'Пари', -1, NULL, false);
INSERT INTO bet (tag, name, bettype, scoretype, timetype, description, mapping_code, xml, enabled) VALUES ('HANERR', 'Фора', 'HAN', 'SCR', 'ERR', 'Пари', -1, NULL, false);
INSERT INTO bet (tag, name, bettype, scoretype, timetype, description, mapping_code, xml, enabled) VALUES ('NXGERR', 'Следующий гол', 'NXG', 'SCR', 'ERR', 'Пари', -1, NULL, false);
INSERT INTO bet (tag, name, bettype, scoretype, timetype, description, mapping_code, xml, enabled) VALUES ('ODDERR', 'Нечет/чет', 'ODD', 'SCR', 'ERR', 'Пари', -1, NULL, false);
INSERT INTO bet (tag, name, bettype, scoretype, timetype, description, mapping_code, xml, enabled) VALUES ('TTLERR', 'Тотал', 'TTL', 'SCR', 'ERR', 'Пари', -1, NULL, false);
INSERT INTO bet (tag, name, bettype, scoretype, timetype, description, mapping_code, xml, enabled) VALUES ('WINERR', 'Победитель', 'WIN', 'SCR', 'ERR', 'Пари', -1, NULL, false);
INSERT INTO bet (tag, name, bettype, scoretype, timetype, description, mapping_code, xml, enabled) VALUES ('EXAERR', 'Точный счет', 'EXA', 'SCR', 'ERR', 'Пари', -1, NULL, false);
--
-- Name: pk_bet; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace:
--

ALTER TABLE ONLY bet
    ADD CONSTRAINT pk_bet PRIMARY KEY (tag);


--
-- Name: pk_bet_type; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace:
--

ALTER TABLE ONLY bet_type
    ADD CONSTRAINT pk_bet_type PRIMARY KEY (shortname);


--
-- Name: pk_score_type; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace:
--

ALTER TABLE ONLY score_type
    ADD CONSTRAINT pk_score_type PRIMARY KEY (shortname);


--
-- Name: pk_time_type; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace:
--

ALTER TABLE ONLY time_type
    ADD CONSTRAINT pk_time_type PRIMARY KEY (tag);


--
-- Name: bet_type_index_shortname; Type: INDEX; Schema: public; Owner: -; Tablespace:
--

CREATE INDEX bet_type_index_shortname ON bet_type USING hash (shortname);


--
-- Name: fki_bet; Type: INDEX; Schema: public; Owner: -; Tablespace:
--

CREATE INDEX fki_bet ON bet USING hash (bettype);


--
-- Name: fki_score; Type: INDEX; Schema: public; Owner: -; Tablespace:
--

CREATE INDEX fki_score ON bet USING hash (scoretype);


--
-- Name: fki_time; Type: INDEX; Schema: public; Owner: -; Tablespace:
--

CREATE INDEX fki_time ON bet USING hash (timetype);


--
-- Name: bet_type; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY bet
    ADD CONSTRAINT bet_type FOREIGN KEY (bettype) REFERENCES bet_type(shortname) ON UPDATE CASCADE;


--
-- Name: score; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY bet
    ADD CONSTRAINT score FOREIGN KEY (scoretype) REFERENCES score_type(shortname) ON UPDATE CASCADE;


--
-- Name: time; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY bet
    ADD CONSTRAINT "time" FOREIGN KEY (timetype) REFERENCES time_type(tag) ON UPDATE CASCADE;


--
-- Name: bet; Type: ACL; Schema: public; Owner: -
--






-----------------------------------------------
----- Line Tables
-----------------------------------------------

-- MultiStringGroup
CREATE TABLE MultiStringGroup
(
	MultiStringGroupID BIGSERIAL NOT NULL PRIMARY KEY,
	MultiStringGroupTag varchar(32) NOT NULL,
	Comment varchar(255) NULL,
	LastModified timestamp NULL,
	SvrMultiStringGroupID int NULL
);

CREATE INDEX IDX_MULTISTRINGGROUP_SvrMultiStringGroupID on MultiStringGroup USING hash (SvrMultiStringGroupID);

--  MultiString
CREATE TABLE MultiString
(
	MultiStringID BIGSERIAL NOT NULL PRIMARY KEY,
	MultiStringTag varchar(255) NULL,
	Comment varchar(255) NULL,
	MultiStringGroupID bigint NULL,
	LastModified timestamp NULL,
	SvrMultiStringID bigint NULL,
	IsLiveBet smallint NULL,
	FOREIGN KEY (MultiStringGroupID) REFERENCES MultiStringGroup (MultiStringGroupID)
);

CREATE INDEX IDX_MULTISTRING_SvrMultiStringID on MultiString USING hash (SvrMultiStringID);
CREATE INDEX IDX_MULTISTRING_complex on MultiString USING hash (MultiStringTag);

-- Language
CREATE TABLE Language
(
	LanguageID BIGSERIAL NOT NULL PRIMARY KEY,
	ShortName varchar(255) NULL,
	MultiStringID bigint NULL,
	Priority int NOT NULL,
	LastModified timestamp NULL,
	SvrLanguageID bigint NULL,
	IsTerminalLanguage int NULL,
	FOREIGN KEY (MultiStringID) REFERENCES MultiString (MultiStringID)
);

CREATE INDEX IDX_LANGUAGE_SvrLanguageID on Language USING hash (SvrLanguageID);

-- LanguageString
CREATE TABLE LanguageString
(
	LanguageStringID BIGSERIAL NOT NULL PRIMARY KEY,
	Text varchar(500) NULL,
	MultiStringID bigint NULL,
	LanguageID bigint NULL,
	LastModified timestamp NULL,
	SvrLanguageStringID bigint NULL,
	IsLiveBet smallint NULL,
	FOREIGN KEY (MultiStringID) REFERENCES MultiString (MultiStringID) MATCH SIMPLE ON UPDATE CASCADE ON DELETE CASCADE,
	FOREIGN KEY (LanguageID) REFERENCES Language (LanguageID) MATCH SIMPLE ON UPDATE CASCADE ON DELETE CASCADE
);

CREATE INDEX IDX_LanguageString_SvrLanguageStringID ON LanguageString USING hash (SvrLanguageStringID);
CREATE INDEX idx_languagestring_languageid ON languagestring USING hash (languageid);
CREATE INDEX idx_languagestring_multistringid ON languagestring USING hash (multistringid);


-- Country
CREATE TABLE Country
(
	CountryId BIGSERIAL NOT NULL PRIMARY KEY,
	ISO2 varchar(255) NULL,
	ISO3 varchar(255) NULL,
	DefaultName varchar(255) NULL,
	MultiStringID bigint NULL,
	LastModified timestamp NULL,
	SvrCountryID bigint NULL,
	FOREIGN KEY (MultiStringID) REFERENCES MultiString (MultiStringID)
);

CREATE INDEX IDX_Country_SvrCountryID ON Country (SvrCountryID);


-- Sport
CREATE TABLE Sport
(
	SportID BIGSERIAL NOT NULL PRIMARY KEY,
	BtrSportID int NOT NULL,
	DefaultName varchar(255) NULL,
	MultiStringID bigint NULL,
	LastModified timestamp NULL,
	ShortName varchar(255) NULL,
	SvrSportID bigint NULL,
	IsLiveBet smallint NULL,
	Deleted smallint NULL,
	FOREIGN KEY (MultiStringID) REFERENCES MultiString (MultiStringID)
);

CREATE INDEX IDX_Sport_SvrSportID ON Sport USING hash (SvrSportID);


-- Category
CREATE TABLE Category
(
	CategoryID BIGSERIAL NOT NULL PRIMARY KEY,
	MultiStringID bigint NULL,
	DefaultName varchar(255) NULL,
	LastModified timestamp NULL,
	Sort int NULL,
	SvrCategoryID bigint NULL,
	IsLiveBet smallint NULL,
        Deleted smallint NULL,
	FOREIGN KEY (MultiStringID) REFERENCES MultiString (MultiStringID)
);

CREATE INDEX IDX_Category_SvrCategoryID ON Category USING hash (SvrCategoryID);


-- Tournament
CREATE TABLE Tournament
(
	TournamentID BIGSERIAL NOT NULL PRIMARY KEY,
	DefaultName varchar(255) NULL,
	BtrTournamentID int NULL,
	MultiStringID bigint NULL,
	SportID bigint NOT NULL,
	CategoryID bigint NOT NULL,
	Active smallint NULL,
	MaxStakeLigaLimit decimal(12, 2) NULL,
	MaxStakeTipLimit decimal(12, 2) NULL,
	MinCombination int NULL,
	LastModified timestamp NULL,
	OddGraduation decimal(12, 2) NULL,
	Sort int NULL,
	MultiStringID2 bigint NULL,
	ScoreFrequency int NULL,
	Info varchar(255) NULL,
	CountryCountryId int NULL,
	ExpireDate timestamp NULL,
	SvrTournamentID int NULL,
	TennisSets int NULL,
	ShowOnOddSheet smallint NULL,
	OutrightTyp int NULL,
	IsLiveBet smallint NULL,
	Deleted smallint NULL,
	IsLocked smallint NULL,
	InfoMultiStringID int NULL,
	AutoUpdateOdds int NULL,
	LockWithAllOtherTournaments int NULL,
	OddsGenerationModel int NULL,
	DoNotUpdateSpecialBetDomains varchar(255) NULL,
	AutoEnablePreMatch int NULL,
	FOREIGN KEY (MultiStringID) REFERENCES MultiString (MultiStringID),
	FOREIGN KEY (MultiStringID2) REFERENCES MultiString (MultiStringID),
	FOREIGN KEY (SportID) REFERENCES Sport (SportID) MATCH SIMPLE ON UPDATE CASCADE ON DELETE CASCADE,
	FOREIGN KEY (CategoryID) REFERENCES Category (CategoryID) MATCH SIMPLE ON UPDATE CASCADE ON DELETE CASCADE
);

CREATE INDEX IDX_Tournament_SvrTournamentID ON Tournament USING hash (SvrTournamentID);
CREATE INDEX idx_tournament_categoryid ON tournament USING hash (categoryid);
CREATE INDEX idx_tournament_sportid ON tournament USING hash (sportid);

-- TournamentLock
CREATE TABLE TournamentLock
(
	TournamentLockID BIGSERIAL NOT NULL PRIMARY KEY,
	TournamentID int NOT NULL,
	ToLockID int NOT NULL,
	TournamentName varchar(255) NULL,
	LastModified timestamp NULL,
	FOREIGN KEY (TournamentID) REFERENCES Tournament (TournamentID) MATCH SIMPLE ON UPDATE CASCADE ON DELETE CASCADE
);

CREATE INDEX IDX_TournamentLock_ToLockID ON TournamentLock(ToLockID);
CREATE INDEX idx_tournamentlock_tournamentid ON tournamentlock USING hash (tournamentid );

-- Competitor
CREATE TABLE Competitor
(
	CompetitorID BIGSERIAL NOT NULL PRIMARY KEY,
	BtrCompetitorID int NULL,
	DefaultName varchar(255) NULL,
	SportID bigint NOT NULL,
	MultiStringID bigint NULL,
	CountryId bigint NULL,
	LastModified timestamp NULL,
	BetTaxTypBetTaxTypID int NULL,
	SvrCompetitorID int NULL,
	LocalCountyTaxLocationID int NULL,
	IsLiveBet smallint NULL,
	Deleted smallint NULL,
	BtrLiveBetCompetitorID int NULL,
	FOREIGN KEY (SportID) REFERENCES Sport (SportID) MATCH SIMPLE ON UPDATE CASCADE ON DELETE CASCADE,
	FOREIGN KEY (MultiStringID) REFERENCES MultiString (MultiStringID),
	FOREIGN KEY (CountryId) REFERENCES Country (CountryId)
);

CREATE INDEX IDX_Competitor_SvrCompetitorID ON Competitor USING hash (SvrCompetitorID);
CREATE INDEX idx_competitor_sportid ON competitor USING hash (sportid);

-- Match
CREATE TABLE Match
(
	MatchId BIGSERIAL NOT NULL PRIMARY KEY,
	StartDate timestamp NULL,
	EndDate timestamp NULL,
	ExpiryDate timestamp NULL,
	MinCombination int NULL,
	TeamWon varchar(255) NULL,
	PointsTeamHome int NULL,
	PointsTeamAway int NULL,
	PointsTeamAwayHalf1 int NULL,
	PointsTeamHomeHalf1 int NULL,
	BtrMatchID int NULL,
	State int NOT NULL,
	Code int NULL,
	Active smallint NOT NULL,
	WeekOfYear int NOT NULL,
	LastModified timestamp NULL,
	TournamentID int NOT NULL,
	UserID int NULL,
	ResultID int NULL,
	BetDomainID int NULL,
	SvrMatchID int NULL,
	ActivateAfterDBSync smallint NULL,
	IsLiveBet smallint NULL,
	Deleted smallint NULL,
	MatchMinute int NULL,
	PeriodType int NULL,
	LB_PeriodInfo int NULL,
	LB_SportType int NULL,
	LiveBetStatus int NULL,
	Info character varying NULL,
	RequestResults smallint NULL,
	CardsTeam1 int NULL,
	CardsTeam2 int NULL,
	Sort int NULL,
	Code_External varchar(255) NULL,
	InfoMultiStringID int NULL,
	MatchIcon varchar(255) NULL,
	MatchScore varchar(255) NULL,
	FOREIGN KEY (TournamentID) REFERENCES Tournament (TournamentID) MATCH SIMPLE ON UPDATE CASCADE ON DELETE CASCADE
);

CREATE INDEX IDX_Match_SvrMatchID ON Match USING hash (SvrMatchID);
CREATE INDEX idx_match_tournamentid ON match USING hash (tournamentid);


-- BetDomain
CREATE TABLE BetDomain
(
	BetDomainID BIGSERIAL NOT NULL PRIMARY KEY,
	Status int NULL,
	BetTag varchar(20) REFERENCES bet(tag) NOT NULL,
	BetDomainNumber int NOT NULL,
	MultiStringID int NULL,
	MultiStringID2 int NULL,
	ScoreFrequency int NULL,
	Sort int NOT NULL,
	HomeHandicap int NULL,
	AwayHandicap int NULL,
	LastModified timestamp NOT NULL,
	ID int NULL,
	Set int NULL,
	MatchId int NOT NULL,
	SvrBetDomainID int NULL,
	MinCombination int NULL,
	Calculated smallint NULL,
	OverAllScore decimal(12, 2) NULL,
	IsLiveBet smallint NULL,
	Deleted smallint NULL,
	BtrLiveBetID int NULL,
	SpecialOddValue varchar(255) NULL,
	PeriodNumber int NULL,
	ResultResultID int NULL,
	IsManualLiveBetDomain smallint NULL,
	SpecialLiveOddValue_Full varchar(100) NULL,
	FOREIGN KEY (MultiStringID) REFERENCES MultiString (MultiStringID),
	FOREIGN KEY (MultiStringID2) REFERENCES MultiString (MultiStringID),
	FOREIGN KEY (MatchId) REFERENCES Match (MatchId) MATCH SIMPLE ON UPDATE CASCADE ON DELETE CASCADE
);

CREATE INDEX IDX_BetDomain_SvrBetDomainID on BetDomain USING hash (SvrBetDomainID);
CREATE INDEX idx_betdomain_bettag ON betdomain USING hash (bettag COLLATE pg_catalog."default");
CREATE INDEX idx_betdomain_matchid ON betdomain USING hash (matchid);

-- Odd
CREATE TABLE Odd
(
	OddID BIGSERIAL NOT NULL PRIMARY KEY,
	Value decimal(12, 2) NULL,
	ScanCode varchar(255) NULL,
	Calculated smallint NULL,
	Won smallint NULL,
	Sort int NOT NULL,
	UserID int NULL,
	MultiStringID int NULL,
	MultiStringID2 int NULL,
	BetDomainID int NOT NULL,
	Active smallint NULL,
	LastModified timestamp NULL,
	SvrOddID int NULL,
	SvrOutcomeId int NULL,
	IsLiveBet smallint NULL,
	Deleted smallint NULL,
	Status int NULL,
	IsLocked smallint NULL,
	OddTag character varying NULL,
	FOREIGN KEY (MultiStringID) REFERENCES MultiString (MultiStringID),
	FOREIGN KEY (MultiStringID2) REFERENCES MultiString (MultiStringID),
	FOREIGN KEY (BetDomainID) REFERENCES BetDomain (BetDomainID) MATCH SIMPLE ON UPDATE CASCADE ON DELETE CASCADE
);

CREATE INDEX IDX_Odd_SvrOddID ON Odd USING hash (SvrOddID);
CREATE INDEX idx_odd_svr_live ON odd USING btree (svroutcomeid, islivebet);
CREATE INDEX idx_odd_betdomainid ON odd USING hash (betdomainid);


-- MatchToCompetitor
CREATE TABLE MatchToCompetitor
(
	Match2CompetitorID BIGSERIAL NOT NULL PRIMARY KEY,
	HomeTeam int NULL,
	CompetitorID int NOT NULL,
	MatchId int NOT NULL,
	IsLiveBet smallint NULL,
	FOREIGN KEY (CompetitorID) REFERENCES Competitor (CompetitorID) MATCH SIMPLE ON UPDATE CASCADE ON DELETE CASCADE,
	FOREIGN KEY (MatchId) REFERENCES Match (MatchId) MATCH SIMPLE ON UPDATE CASCADE ON DELETE CASCADE
);
CREATE INDEX idx_matchtocompetitor_competitorid ON matchtocompetitor USING hash (competitorid);
CREATE INDEX idx_matchtocompetitor_matchid ON matchtocompetitor USING hash (matchid);

-------------------------------------------------------
------ Terminal & Shop Tables
-------------------------------------------------------

-- UpdateFileEntry
CREATE TABLE UpdateFileEntry
(
	UpdateFileID BIGSERIAL NOT NULL PRIMARY KEY,
	UpdateFile varchar(255) NULL,
	ExpireDate timestamp NULL,
	CreateDate timestamp NULL,
	FranchisorID bigint NULL,
	LastExportDate timestamp NULL
);

-- NBTLog
CREATE TABLE NBTLog
(
	NBTLogID BIGSERIAL NOT NULL PRIMARY KEY,
	Text text NULL,
	ObjectID varchar(255) NULL,
	Criticality smallint NOT NULL,
	Confirmed smallint NOT NULL,
	InsertDate timestamp NULL,
	GroupCriteria varchar(255) NULL,
	IsTransmited smallint NULL
);

-- StationCash
CREATE TABLE StationCash
(
	StationCashID BIGSERIAL NOT NULL PRIMARY KEY,
	Cash decimal (10, 2) NOT NULL,
	MoneyIn smallint NOT NULL,
	OperationType text NULL,
	OperatorID varchar(255) NULL,
	CashCheckPoint smallint NOT NULL,
	DateModified timestamp NOT NULL
);

-- TransactionQueue
CREATE TABLE TransactionQueue
(
	TransactionQueueID BIGSERIAL NOT NULL PRIMARY KEY,
	Type smallint NOT NULL,
	Description text NOT NULL,
	TransactionId varchar(1024) NULL,
	UidState text NULL,
	Object1 text NULL,
	Object2 text NULL,
	Tag1 text NULL,
	Tag2 text NULL,
	Created timestamp NOT NULL
);


-- StationAppConfig
CREATE TABLE StationAppConfig
(
	StationAppConfigID BIGSERIAL NOT NULL PRIMARY KEY,
	Username varchar(255) NULL,
	Password varchar(255) NULL,
	CalcOffLineTime timestamp NULL,
	LastSyncTime timestamp NULL,
	LastOperationTime timestamp NULL,
	LastLimitResetTime timestamp NULL,
	IsFirstStart smallint NOT NULL,
	StationNumber varchar(255) NULL,
	LastTicketNbr varchar(255) NULL,
	LastLiveBetResetTime timestamp NULL,
	CashPoolValue varchar(255) NULL,
	LiveBetOffLineStartTime timestamp NULL,
	UndefinedCol int NULL,
	LastLoggedInUser varchar(255) NULL,
	OfflineMoneyDeposit decimal(10, 2) NULL,
	LastTransactionIndex bigint NULL
);

-- StationAppConfig
CREATE TABLE Station
(
	StationID BIGSERIAL NOT NULL PRIMARY KEY,
	Number varchar(255) NOT NULL,
	NotinUse smallint NULL,
	IsAssigned smallint NULL,
	Discriminator varchar(255) NULL,
	FranchisorID int NULL,
	LocationID int NULL,
	TechnicianID int NULL,
	SyncInterval int NULL,
	AllowOffLine smallint NULL,
	MaxOffLineTime int NULL,
	SendLogFile smallint NULL,
	SerialNumber varchar(255) NULL,
	Telefon varchar(255) NULL,
	Info varchar(255) NULL,
	MaxCombination int NULL,
	MinCombination int NULL,
	MaxStakeCombi decimal(12, 2) NULL,
	MaxSystemBet int NULL,
	MaxStakeSystemBet decimal(12, 2) NULL,
	MaxWinSystemBet decimal(12, 2) NULL,
	AllowMultiBet smallint NULL,
	ShowU18Msg smallint NULL,
	ShowAddictionMsg smallint NULL,
	ClosingTimeBeforeMatch int NULL,
	BonusFromOdd decimal(12, 2) NULL,
	MinStakeSingleBet decimal(12, 2) NULL,
	MaxStakeSingleBet decimal(12, 2) NULL,
	MaxOdd decimal(12, 2) NULL,
	MaxWinSingleBet decimal(12, 2) NULL,
	LockOfferOnLimit smallint NULL,
	BetTerms text NULL,
	HeaderLine1 text NULL,
	HeaderLine2 text NULL,
	FooterLine1 text NULL,
	FooterLine2 text NULL,
	DefaultStationID int NULL,
	HasDefault smallint NULL,
	TimeZoneDiff decimal(12, 2) NULL,
	CanIgnoreCombiLimit smallint NULL,
	MinStakeCombiBet decimal(12, 2) NULL,
	MinStakeSystemBet decimal(12, 2) NULL,
	Active int NULL,
	ModifyData timestamp NULL,
	AccountID int NULL,
	LastUpdateRequest timestamp NULL,
	StationName varchar(255) NULL,
	DaysNotPayedWins int NULL,
	StationTyp varchar(255) NULL,
	SendLogFileDate timestamp NULL,
	FeePercent decimal(12, 2) NULL,
	StationRate decimal(12, 2) NULL,
	HoldPercent decimal(12, 2) NULL,
	IsAccountingAddressFromFranchisor smallint NULL,
	OtherAccountingFranchisorID int NULL,
	AccountingFranchisorName varchar(255) NULL,
	AccountingStreet varchar(255) NULL,
	AccountingZipCode varchar(255) NULL,
	AccountingTown varchar(255) NULL,
	AccountingToHand varchar(255) NULL,
	OtherFranchisorHoldPercent decimal(12, 2) NULL,
	FranchisorIndex int NULL,
	AdminCode int NULL,
	FranchisorCode int NULL,
	AccountingCode int NULL,
	TestModeCode int NULL,
	BookingNbr varchar(255) NULL,
	IsAuthorized smallint NULL,
	AuthorizeDate timestamp NULL,
	OpenAuthorizeDate timestamp NULL,
	StationTypName varchar(255) NULL,
	AccountingAddOnInfo varchar(255) NULL,
	SyncDBFromDate varchar(255) NULL,
	SetSyncDBFromDate smallint NULL,
	RequestNewDatebaseAfterHours int NULL,
	ShouldSendDatabase smallint NULL,
	SendDatabaseAfterHours int NULL,
	HasLiveBet smallint NULL,
	HasSportBet smallint NULL,
	FinalAccountDate varchar(255) NULL,
	UseCashButtons smallint NULL,
	PrintLogo smallint NULL,
	CombiLiveBetTicketAcceptTime int NULL,
	SngLiveBetTicketAcceptTime int NULL,
	LastViewIdleTime int NULL,
	OddGraduation decimal(12, 2) NULL,
	OddGraduationStatus int NULL,
	DoRestart int NULL,
	DoExcecuteCmd varchar(255) NULL,
	ShowNotPayedWins int NULL,
	UseCashPool int NULL,
	ShowTerminalLogin int NULL,
	OnlineTimeAfterLogin int NULL,
	TerminalLoginCode int NULL,
	ImportFullDB int NULL,
	SendSMSOnStartUp int NULL,
	HeaderLine3 varchar(255) NULL,
	HeaderLine4 varchar(255) NULL,
	AuthorizationInfo varchar(255) NULL,
	Calculate_Fee int NULL,
	SplitAccounting int NULL,
	CreditNoteExpirationDays int NULL,
	BarcodeScannerAlwayActive smallint NULL,
	CashAcceptorAlwayActive smallint NULL,
	created_at timestamp NULL
);

CREATE TABLE OfferDisable
(
	OfferDisableID BIGSERIAL NOT NULL PRIMARY KEY,
	TicketNumber varchar(255) NULL,
	StationNumber varchar(255) NULL,
	SvrTournamentID BIGINT NULL,
	SvrOddID BIGINT NULL,
	BetDomainText varchar(255) NULL,
	TipText varchar(255) NULL,
	LockDate timestamp NULL,
	EnableDate timestamp NULL,
	LastModified timestamp NULL,
	IsEnabled bit NULL
);

CREATE TABLE StationMessage
(
	StationMessageID BIGSERIAL NOT NULL PRIMARY KEY,
	InsertDate timestamp NULL,
	Text text  NULL
);

CREATE TABLE NbtUserLight
(
	NbtUserLightID BIGSERIAL NOT NULL PRIMARY KEY,
	FirstName varchar(255) NULL,
	LastName varchar(255) NULL,
	LoginName varchar(255) NULL,
	PasswordHash varchar(255) NULL,
	LoginMessage varchar(255) NULL,
	IsSystem smallint NULL,
	IsLocked smallint NULL,
	ChangePassword smallint NULL,
	LastLoggedIn timestamp NULL,
	LastLoggedOut timestamp NULL
);

CREATE TABLE settings
(
    settings_id BIGSERIAL NOT NULL PRIMARY KEY,
 name varchar(255) NOT NULL,
 type varchar(32) NOT NULL,
 value_string TEXT NULL,
 value_long BIGINT NULL,
 value_decimal BIGINT NULL,
 value_date timestamp NULL
);

CREATE UNIQUE INDEX settings_index_name ON settings (name);

--CREATE OR REPLACE FUNCTION "processBetDomain"("V_matchid" integer, "V_dt" timestamp with time zone, "V_mstr" integer, "V_live" integer, tag character varying, "groupPosition" integer, "groupId" integer, "adValue" character varying)
--  RETURNS integer AS
--$BODY$BEGIN
--DECLARE
--    V_bdm integer;
--begin
--    INSERT INTO BetDomain(Status, BetTag, BetDomainNumber, MultiStringID, MultiStringID2, ScoreFrequency, Sort, HomeHandicap, AwayHandicap, LastModified, ID, Set, MatchId, SvrBetDomainID, MinCombination, Calculated, OverAllScore, IsLiveBet, BtrLiveBetID, SpecialOddValue, PeriodNumber, ResultResultID, IsManualLiveBetDomain, SpecialLiveOddValue_Full)
--    VALUES (0, "tag", 387, "V_mstr", "V_mstr", NULL, "groupPosition", NULL,NULL, "V_dt", NULL, 0, "V_matchid", "groupId", 0, 0, NULL, "V_live", NULL, "adValue", NULL, NULL, 0, NULL) RETURNING BetDomainID INTO V_bdm;
--    RETURN V_bdm;
--end;
--END;$BODY$
--  LANGUAGE plpgsql VOLATILE
--  COST 100;

CREATE OR REPLACE FUNCTION "processBetDomain"("V_matchid" integer, "V_dt" timestamp with time zone, "V_mstr" integer, "V_live" integer, "betType" character varying, "timeType" character varying, "groupPosition" integer, "groupId" integer, "adValue" character varying)
  RETURNS integer AS
$BODY$BEGIN
DECLARE
V_tag character varying;
V_bdm integer;
begin

select tag INTO V_tag from bet where bettype="betType" and scoretype='SCR' and timetype="timeType";
IF NOT FOUND THEN
   BEGIN
	INSERT INTO bet(tag, name, bettype, scoretype, timetype, description, mapping_code, xml, enabled)
	SELECT "betType" || "timeType", bt.name, bt.shortname, 'SCR', tt.tag, bt.name || ' (' || tt.name || ')', -1, NULL, TRUE FROM bet_type bt INNER JOIN time_type tt on bt.shortname="betType" and tt.tag="timeType"
	RETURNING tag INTO V_tag;
   END;
END IF;
INSERT INTO BetDomain(Status, BetTag, BetDomainNumber, MultiStringID, MultiStringID2, ScoreFrequency, Sort, HomeHandicap, AwayHandicap, LastModified, ID, Set, MatchId, SvrBetDomainID, MinCombination, Calculated, OverAllScore, IsLiveBet, BtrLiveBetID, SpecialOddValue, PeriodNumber, ResultResultID, IsManualLiveBetDomain, SpecialLiveOddValue_Full)
VALUES (0, V_tag, 387, "V_mstr", "V_mstr", NULL, "groupPosition", NULL,NULL, "V_dt", NULL, 0, "V_matchid", "groupId", 0, 0, NULL, "V_live", NULL, "adValue", NULL, NULL, 0, NULL) RETURNING BetDomainID INTO V_bdm;
RETURN V_bdm;
end;
END;$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;

CREATE OR REPLACE FUNCTION "processBetDomain"("V_matchid" integer, "V_dt" timestamp with time zone, "V_live" integer, "betType" character varying, "timeType" character varying, "groupPosition" integer, "groupId" integer, "adValue" character varying, "V_ru" integer, "title" character varying)
  RETURNS integer AS
$BODY$BEGIN
DECLARE
V_tag character varying;
V_bdm integer;
V_mstr integer;
begin

select tag INTO V_tag from bet where bettype="betType" and scoretype='SCR' and timetype="timeType";
IF NOT FOUND THEN
   BEGIN
        INSERT INTO bet_type(shortname, name, description) SELECT "betType", '-', '-' WHERE NOT EXISTS (SELECT bt.shortname FROM bet_type bt WHERE bt.shortname = "betType" LIMIT 1);
	INSERT INTO bet(tag, name, bettype, scoretype, timetype, description, mapping_code, xml, enabled)
	SELECT "betType" || "timeType", bt.name, bt.shortname, 'SCR', tt.tag, bt.name || ' (' || tt.name || ')', -1, NULL, TRUE FROM bet_type bt INNER JOIN time_type tt on bt.shortname="betType" and tt.tag="timeType"
	RETURNING tag INTO V_tag;
   END;
END IF;
SELECT "processMultiString"("V_ru", "V_live", "V_dt", "title") INTO V_mstr;
INSERT INTO BetDomain(Status, BetTag, BetDomainNumber, MultiStringID, MultiStringID2, ScoreFrequency, Sort, HomeHandicap, AwayHandicap, LastModified, ID, Set, MatchId, SvrBetDomainID, MinCombination, Calculated, OverAllScore, IsLiveBet, BtrLiveBetID, SpecialOddValue, PeriodNumber, ResultResultID, IsManualLiveBetDomain, SpecialLiveOddValue_Full)
VALUES (0, V_tag, 387, V_mstr, V_mstr, NULL, "groupPosition", NULL,NULL, "V_dt", NULL, 0, "V_matchid", "groupId", 0, 0, NULL, "V_live", NULL, "adValue", NULL, NULL, 0, NULL) RETURNING BetDomainID INTO V_bdm;
RETURN V_bdm;
end;
END;$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;

CREATE OR REPLACE FUNCTION "processCompetitor"("V_sid" integer, "V_matchid" integer, "V_live" integer, tnumber integer, tag character varying)
  RETURNS integer AS
$BODY$BEGIN
DECLARE
    V_cmpid integer;
begin
    SELECT CompetitorID INTO V_cmpid FROM competitor WHERE DefaultName = "tag" AND SportID = "V_sid" AND IsLiveBet = "V_live";
    IF NOT FOUND THEN
        BEGIN
            insert into competitor(DefaultName, SportID, CountryID, SvrCompetitorID, IsLiveBet) VALUES("tag", "V_sid", NULL, 0, "V_live") RETURNING CompetitorID INTO V_cmpid;
        END;
    END IF;
    insert into MatchToCompetitor(HomeTeam, CompetitorID, MatchID, IsLiveBet) VALUES("tnumber", V_cmpid, "V_matchid", "V_live");
    RETURN V_cmpid;
end;
END;$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;

CREATE OR REPLACE FUNCTION "processMultiString"("V_ru" integer, "V_live" integer, "V_dt" timestamp with time zone, tag character varying)
  RETURNS integer AS
$BODY$BEGIN
DECLARE
    V_mstr integer;
begin
    SELECT MultiStringID INTO V_mstr FROM MultiString WHERE IsLiveBet = "V_live" AND MultiStringTag = "tag";
    IF NOT FOUND THEN
	BEGIN
	    INSERT INTO MultiString (IsLiveBet, MultiStringTag, LastModified) VALUES ("V_live", "tag", "V_dt") RETURNING MultiStringID INTO V_mstr;
	    INSERT INTO LanguageString (Text, MultiStringID, LanguageID, LastModified, SvrLanguageStringID, IsLiveBet) VALUES ("tag", V_mstr, "V_ru", "V_dt", NULL, "V_live");
        END;
    END IF;
    RETURN V_mstr;
end;
END;$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;

CREATE OR REPLACE FUNCTION "processOdd"("V_bdm" integer, "V_dt" timestamp with time zone, "V_mstr" integer, "V_live" integer, tag character varying, value numeric, "position" integer, "betEnabled" integer, "FacId" integer, "OutcomeId" integer)
  RETURNS void AS
$BODY$BEGIN
    INSERT INTO Odd (Value ,ScanCode ,Calculated, Won ,Sort ,UserID ,MultiStringID, MultiStringID2 ,BetDomainID ,Active, LastModified, SvrOddID, SvrOutcomeID, IsLiveBet, Status, IsLocked, OddTag)
    VALUES ("value", NULL, 0, 0, "position", 1, "V_mstr", "V_mstr", "V_bdm", "betEnabled", "V_dt", "FacId", "OutcomeId", "V_live", NULL, NULL, "tag");
END;$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;

CREATE OR REPLACE FUNCTION "processOdd"("V_bdm" integer, "V_ru" integer, "V_dt" timestamp with time zone, "V_live" integer, tag character varying, title character varying, value numeric, "position" integer, "betEnabled" integer, "FacId" integer, "OutcomeId" integer)
  RETURNS void AS
$BODY$BEGIN
DECLARE
    V_mstr integer;
begin
    SELECT "processMultiString"("V_ru", "V_live", "V_dt", "title") INTO V_mstr;
    INSERT INTO Odd (Value ,ScanCode ,Calculated, Won ,Sort ,UserID ,MultiStringID, MultiStringID2 ,BetDomainID ,Active, LastModified, SvrOddID, SvrOutcomeID, IsLiveBet, Status, IsLocked, OddTag)
    VALUES ("value", NULL, 0, 0, "position", 1, V_mstr, V_mstr, "V_bdm", "betEnabled", "V_dt", "FacId", "OutcomeId", "V_live", NULL, NULL, "tag");
end;
END;$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;

CREATE OR REPLACE FUNCTION "createTournament"("V_sid" integer, "V_cid" integer, "V_dt" timestamp with time zone, "V_live" integer, "title" character varying, "topicId" integer)
  RETURNS integer AS
$BODY$BEGIN
DECLARE
V_tid integer;
begin
INSERT INTO tournament(SportID,CategoryID,DefaultName,LastModified,Sort,SvrTournamentID,Active,IsLiveBet,MinCombination)
VALUES("V_sid","V_cid","title","V_dt",1,"topicId",1,"V_live",0) RETURNING TournamentId INTO V_tid;
RETURN V_tid;
end;
END;$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;

CREATE OR REPLACE FUNCTION "createTournament"("V_dt" timestamp with time zone, "V_live" integer, "title" character varying, "topicId" integer, "parentId" bigint)
  RETURNS void AS
$BODY$BEGIN
begin
INSERT INTO tournament(SportID, CategoryID, DefaultName, LastModified, Sort, SvrTournamentID, Active, IsLiveBet, MinCombination)
SELECT s.SportID, c.CategoryId, "title", "V_dt", 1, "topicId", 1, "V_live", 0 FROM sport s
INNER JOIN category c ON c.SvrCategoryID = s.SvrSportID AND s.SvrSportID="parentId" AND c.IsLiveBet = "V_live" AND s.IsLiveBet = "V_live";
end;
END;$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;

CREATE OR REPLACE FUNCTION "checkLanguage"("V_live" integer, "V_dt" timestamp with time zone, "sname" character varying, "fname" character varying)
  RETURNS integer AS
$BODY$BEGIN
DECLARE
V_mstr integer;
V_pri integer;
V_ru integer;
begin
SELECT LanguageID INTO V_ru FROM Language WHERE Language.ShortName="sname";
IF NOT FOUND THEN
   BEGIN
	RAISE INFO '% not found - creating', "sname";
	SELECT MAX(Priority) INTO V_pri FROM Language;
	IF V_pri IS NULL THEN V_pri:=1; ELSE V_pri:=V_pri+1; END IF;
	RAISE INFO '% priority set %', "sname", V_pri;
	INSERT INTO Language(ShortName, MultiStringID, Priority, LastModified, SvrLanguageID, IsTerminalLanguage) VALUES ("sname", NULL, V_pri, "V_dt", NULL, 1) RETURNING LanguageID INTO V_ru;
	INSERT INTO MultiString(IsLiveBet, MultiStringTag, LastModified) VALUES ("V_live", NULL, "V_dt") RETURNING MultiStringID INTO V_mstr;
	INSERT INTO LanguageString(Text, MultiStringID, LanguageID, LastModified, SvrLanguageStringID, IsLiveBet) VALUES ("fname", V_mstr, V_ru, "V_dt", NULL, "V_live");
	UPDATE Language SET MultiStringID=V_mstr WHERE LanguageID=V_ru;
   END;
END IF;
   RAISE INFO '% using existing id %', "sname", V_ru;
RETURN V_ru;
end;
END;$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;

CREATE OR REPLACE FUNCTION "getBetDomain"("matchId" integer, "betId" integer, "V_live" integer)
  RETURNS integer AS
$BODY$BEGIN
DECLARE
V_bdm integer;
begin
SELECT BetDomainID INTO V_bdm FROM BetDomain WHERE matchid = (SELECT MatchID FROM match WHERE SvrMatchID="matchId" AND IsLiveBet="V_live") AND SvrBetDomainID = "betId" AND IsLiveBet="V_live";
RETURN V_bdm;
end;
END;$BODY$
  LANGUAGE plpgsql STABLE
  COST 100;

CREATE OR REPLACE FUNCTION "updateOdd"("oddId" integer, "outcomeId" integer, "val" numeric, "pos" integer, "V_dt" timestamp with time zone, "V_live" integer)
  RETURNS void AS
$BODY$BEGIN
begin
UPDATE Odd SET Value="val", Sort="pos", LastModified="V_dt", SvrOddID="oddId" WHERE SvrOutcomeID="outcomeId" AND IsLiveBet="V_live";
end;
END;$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 200;

CREATE OR REPLACE FUNCTION "updateBetDomainSOV"("outcomeId" integer, "sov" character varying(255), "V_live" integer)
  RETURNS void AS
$BODY$BEGIN
begin
UPDATE BetDomain SET SpecialOddValue="sov" WHERE BetDomainID=(SELECT BetDomainID FROM Odd WHERE SvrOutcomeID="outcomeId" LIMIT 1) AND IsLiveBet="V_live";
end;
END;$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 200;

CREATE OR REPLACE FUNCTION "cleanupSportAndCategory"()
  RETURNS void AS
$BODY$BEGIN
begin
DELETE FROM sport WHERE Deleted=1 AND SvrSportID IS NULL;
DELETE FROM category WHERE Deleted=1 AND SvrCategoryID IS NULL;
end;
END;$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 400;

CREATE OR REPLACE FUNCTION "deleteSportAndCategory"("V_live" integer)
  RETURNS void AS
$BODY$BEGIN
begin
DELETE FROM sport WHERE IsLiveBet = "V_live";
DELETE FROM category WHERE IsLiveBet = "V_live";
end;
END;$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 400;

CREATE OR REPLACE FUNCTION "processLiveMatch"("V_tid" integer, "V_dt" timestamp with time zone, "V_svrmatchid" integer,
"info" character varying, "V_live" integer,
"startDate" timestamp with time zone, "endDate" timestamp with time zone, "expiryDate" timestamp with time zone,
"lineNumber" integer, "matchMinute" integer, "sort" integer,
"hPoints" integer,"aPoints" integer,
"hCards" integer,"aCards" integer,
"matchScore" character varying
)
  RETURNS integer AS
$BODY$BEGIN
DECLARE
V_matchid integer;
begin
insert into match(TournamentID, LastModified, MinCombination,
		  State, Active, SvrMatchID,
		  Info, WeekOfYear, IsLiveBet,
		  StartDate, EndDate, ExpiryDate,
                  Code, MatchMinute, Sort,
                  PointsTeamHome, PointsTeamAway,
                  CardsTeam1, CardsTeam2,
                  MatchScore)
	   VALUES("V_tid", "V_dt", 0,
                  3, 1, "V_svrmatchid",
		  "info", 0, "V_live",
                  "startDate", "endDate", "expiryDate",
                  "lineNumber", "matchMinute","sort",
                  "hPoints","aPoints",
		  "hCards","aCards",
		  "matchScore") RETURNING MatchID INTO V_matchid;
RETURN V_matchid;
end;
END;$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;

CREATE OR REPLACE FUNCTION "updateLiveMatch"("V_dt" timestamp with time zone, "V_svrmatchid" integer,
"matchInfo" character varying, "V_live" integer,
"mstartDate" timestamp with time zone, "mendDate" timestamp with time zone, "mexpiryDate" timestamp with time zone,
"lineNumber" integer, "mmatchMinute" integer, "mSort" integer,
"hPoints" integer,"aPoints" integer,
"hCards" integer,"aCards" integer,
"mmatchScore" character varying
)
  RETURNS integer AS
$BODY$BEGIN
DECLARE
V_matchid integer;
begin
UPDATE match SET Info="matchInfo", Sort="mSort",
		 StartDate="mstartDate", EndDate="mendDate", ExpiryDate="mexpiryDate",
		 MatchMinute="mmatchMinute",
		 pointsteamhome="hPoints",pointsteamaway="aPoints",
		 cardsteam1="hCards",cardsteam2="aCards",
		 matchscore="mmatchScore", LastModified = "V_dt", Code="lineNumber"
    WHERE SvrMatchID="V_svrmatchid" AND IsLiveBet="V_live" RETURNING MatchID INTO V_matchid;
RETURN V_matchid;
end;
END;$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;

CREATE OR REPLACE FUNCTION "processPreMatch"("V_tid" integer, "V_dt" timestamp with time zone, "V_svrmatchid" integer, "V_live" integer,
"startDate" timestamp with time zone, "endDate" timestamp with time zone, "expiryDate" timestamp with time zone, "lineNumber" integer, "sort" integer)
  RETURNS integer AS
$BODY$BEGIN
DECLARE
V_matchid integer;
begin
insert into match(TournamentID, LastModified, MinCombination,
		  State, Active, SvrMatchID,
		  WeekOfYear, IsLiveBet,
		  StartDate, EndDate, ExpiryDate,
                  Code, Sort)
	   VALUES("V_tid", "V_dt", 0,
                  3, 1, "V_svrmatchid",
		  0, "V_live",
                  "startDate", "endDate", "expiryDate",
                  "lineNumber", "sort") RETURNING MatchID INTO V_matchid;
RETURN V_matchid;
end;
END;$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;

CREATE OR REPLACE FUNCTION "updatePreMatch"("V_dt" timestamp with time zone, "V_svrmatchid" integer, "V_live" integer,
"mstartDate" timestamp with time zone, "mendDate" timestamp with time zone, "mexpiryDate" timestamp with time zone, "lineNumber" integer, "msort" integer)
  RETURNS integer AS
$BODY$BEGIN
DECLARE
V_matchid integer;
begin
UPDATE match SET LastModified="V_dt",StartDate="mstartDate",EndDate="mendDate",ExpiryDate="mexpiryDate",Sort="msort",Code="lineNumber"
WHERE SvrMatchID="V_svrmatchid" AND IsLiveBet="V_live" RETURNING MatchID INTO V_matchid;
RETURN V_matchid;
end;
END;$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;

CREATE OR REPLACE FUNCTION "processSport"(OUT sport_id integer, OUT category_id integer, IN "Name" character varying, IN "SName" character varying, IN "svrSportId" bigint, IN "V_dt" timestamp with time zone, IN "V_live" integer)
  RETURNS record AS
$BODY$BEGIN
begin
insert into sport(DefaultName,ShortName,LastModified,SvrSportID,BtrSportID,IsLiveBet)
VALUES("Name","SName", "V_dt","svrSportId","svrSportId","V_live") RETURNING SportID INTO "sport_id";
insert into category(DefaultName,LastModified,Sort,SvrCategoryID,IsLiveBet)
VALUES("Name","V_dt",1,"svrSportId","V_live") RETURNING CategoryId INTO "category_id";
end;
END;$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;

CREATE OR REPLACE FUNCTION "updateBetDomainSOV"("outcomeId" integer, sov character varying, "V_live" integer)
  RETURNS void AS
$BODY$BEGIN
begin
UPDATE BetDomain SET SpecialOddValue="sov" WHERE BetDomainID=(SELECT BetDomainID FROM Odd WHERE SvrOutcomeID="outcomeId" LIMIT 1) AND IsLiveBet="V_live";
end;
END;$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 200;
