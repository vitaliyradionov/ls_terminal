param
(
  [string]$working_directory,
  [string]$revision,
  [int]$time_out=100000
)

$process = New-Object System.Diagnostics.Process

$process.StartInfo.WorkingDirectory       = $working_directory
$process.StartInfo.FileName               = 'git.exe'
$process.StartInfo.Arguments              = 'reset --hard'
$process.StartInfo.UseShellExecute        = $false
$process.StartInfo.RedirectStandardOutput = $true
$process.StartInfo.RedirectStandardError  = $true

$process.Start()
$process.WaitForExit($time_out)

Write-Host $process.StandardOutput.ReadToEnd()
Write-Host $process.StandardError.ReadToEnd() -ForegroundColor Red

$process.Close()




$process = New-Object System.Diagnostics.Process

$process.StartInfo.WorkingDirectory       = $working_directory
$process.StartInfo.FileName               = 'git.exe'
$process.StartInfo.Arguments              = 'pull'
$process.StartInfo.UseShellExecute        = $false
$process.StartInfo.RedirectStandardOutput = $true
$process.StartInfo.RedirectStandardError  = $true

$process.Start()
$process.WaitForExit($time_out)

Write-Host $process.StandardOutput.ReadToEnd()
Write-Host $process.StandardError.ReadToEnd() -ForegroundColor Red

$process.Close()




$process = New-Object System.Diagnostics.Process

$process.StartInfo.WorkingDirectory       = $working_directory
$process.StartInfo.FileName               = 'git.exe'
$process.StartInfo.Arguments              = 'checkout ' + $revision
$process.StartInfo.UseShellExecute        = $false
$process.StartInfo.RedirectStandardOutput = $true
$process.StartInfo.RedirectStandardError  = $true

$process.Start()
$process.WaitForExit($time_out)

Write-Host $process.StandardOutput.ReadToEnd()
Write-Host $process.StandardError.ReadToEnd() -ForegroundColor Red

$process.Close()
